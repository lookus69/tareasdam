/* lookus  */
package di02tarea;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 *
 * @author lookus
 */
public class FormularioController implements Initializable{
  private final String[] secciones = {
    "General",
    "Panadería",
    "Pescadería",
    "Frutería",
    "Carnicería",
    "Charcutería",
    "Conservas",
    "Perfumería"
  };
  @FXML
  TextField txtNombre;
  @FXML
  TextField txtCantidad;
  @FXML
  ChoiceBox<String> selectSeccion;
  @FXML
  CheckBox chkUrgente;

  @Override
  public void initialize(URL url, ResourceBundle rb) {
    selectSeccion.setItems(FXCollections.observableArrayList(secciones));
    selectSeccion.setValue(secciones[0]);
  }//initialize
  
  public void agregarProducto(ActionEvent e) throws Exception{
    final String n = txtNombre.textProperty().get().trim();
    if(n.length() < 1){
      System.out.println("Nombre de producto vacio. [operacion cancelada]");
      cancelar(e);
      return;
    }
    int c;
    try{
      c = Integer.parseInt(txtCantidad.textProperty().get());
    }catch(Exception ex){
      System.out.println("Cantidad no valida. [cantidad por defecto = 1]");
      c = 1;
    }
    final boolean u = chkUrgente.isSelected();
    Producto p = new Producto(n, selectSeccion.getValue(), c, u);
    ProductoController.agregarProducto(p);
    cancelar(e);
  }//agregarProducto()
  
  public void cancelar(ActionEvent e){
     Button b = (Button) e.getSource();
    Stage s = (Stage) b.getScene().getWindow();
    s.close();
  }//cancelar()
  
}//FormularioController
