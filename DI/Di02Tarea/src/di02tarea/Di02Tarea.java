/* lookus  */
package di02tarea;

import java.io.IOException;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

/**
 *
 * @author lookus
 */
public class Di02Tarea extends Application{

  private Stage stage;
  private AnchorPane pane;
  private final String titulo = "DI02 Tarea";
  private final String principal="FXMLprincipal.fxml";
  private Scene escena;

  public static void main(String[] args) {
    launch();
  }//main()

  @Override
  public void start(Stage stage) {
    this.stage = stage;
    this.stage.setTitle(this.titulo);
    abrirVentana();
  }//start()

  public void abrirVentana() {
    try {
      FXMLLoader loader = new FXMLLoader();
      loader.setLocation(this.getClass().getResource(this.principal));
      this.pane = (AnchorPane) loader.load();
      this.escena = new Scene(this.pane);
      this.stage.setScene(escena);
      this.stage.show();
    } catch (IOException e) {
      System.out.println("error al abrir el archivo " + this.principal);
    }
  }//abrirVentana()


}//Di02Tarea
