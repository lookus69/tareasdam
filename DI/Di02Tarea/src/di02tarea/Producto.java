/* lookus  */
package di02tarea;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.scene.control.CheckBox;

/**
 *
 * @author lookus
 */

public class Producto {
  private CheckBox marcado;
  private SimpleStringProperty nombre;
  private SimpleStringProperty seccion;
  private SimpleIntegerProperty cantidad;
  private SimpleBooleanProperty urgente;
  
  
//  enum Seccion{
//  PANADERIA("Panadería"),
//  PESCADERIA("Pescadería"),
//  FRUTERIA("Frutería"),
//  CARNICERIA("Carnicería"),
//  CHARCUTERIA("Charcutería"),
//  CONSERVAS("Conservas"),
//  PERFUMERIA("Perfumería"),
//  GENERAL("General");
//
//  private String seccion;
//  private Seccion (String sec){
//    this.seccion = sec;
//  }
//  @Override
//  public String toString(){
//    return seccion;
//  }
// }//Seccion enum

  public Producto(String nombre, String seccion, int cantidad, boolean urgente) {
    this.marcado = new CheckBox();
    this.nombre = new SimpleStringProperty(nombre);
    this.seccion = new SimpleStringProperty(seccion);
    this.cantidad = new SimpleIntegerProperty(cantidad);
    this.urgente = new SimpleBooleanProperty(urgente);
  }//constructor()
  @Override
  public String toString(){
    final String u = this.urgente.get() ? "urgente!" : "";
    return getCantidad() + " x \t" + getNombre() + " \t" + getSeccion() + " \t" + u;
  }

  public CheckBox getMarcado(){
    return this.marcado;
  }
  
  public void setNombre(String nombre) {
    this.nombre = new SimpleStringProperty(nombre);
  }
  public String getNombre(){
    return this.nombre.get();
  }
  public SimpleStringProperty nombreProperty(){
    return this.nombre;
  }

  public String getSeccion() {
    return seccion.get();
  }
  public void setSeccion(String seccion) {
    this.seccion = new SimpleStringProperty(seccion);
  }
  public SimpleStringProperty seccionProperty(){
    return this.seccion;
  }

  public int getCantidad() {
    return cantidad.get();
  }
  public void setCantidad(int cantidad) {
    this.cantidad = new SimpleIntegerProperty(cantidad);
  }
  public SimpleIntegerProperty cantidadProperty(){
    return this.cantidad;
  }

  public String getUrgente() {
    return urgente.get() ? "x" : "";
  }
  public void setUrgente(boolean urgente) {
    this.urgente = new SimpleBooleanProperty(urgente);
  }

}//Producto
