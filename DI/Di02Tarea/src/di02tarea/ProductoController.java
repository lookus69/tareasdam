/* lookus  */
package di02tarea;

import java.net.URL;
import java.util.Iterator;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.FlowPane;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 *
 * @author lookus
 */
public class ProductoController implements Initializable {
  private final String formulario="FXMLformulario.fxml";
  private final String titulo = "DI02 Tarea - Formulario";
  private static ObservableList<Producto> productos;
  
  @FXML
  public TableView<Producto> listaCompra;
  @FXML
  public TableColumn<Producto, CheckBox> marcado;
  @FXML
  public TableColumn<Producto, String> nombre;
  @FXML
  public TableColumn<Producto, String> seccion;
  @FXML
  public TableColumn<Producto, Integer> cantidad;
  @FXML
  public TableColumn<Producto, String> urgente;
  @FXML
  public Text txtImpresora;

  @Override
  public void initialize(URL url, ResourceBundle rb) {
    productos = FXCollections.observableArrayList();
    marcado.setCellValueFactory(new PropertyValueFactory<>("marcado"));
    nombre.setCellValueFactory(new PropertyValueFactory<>("nombre"));
    seccion.setCellValueFactory(new PropertyValueFactory<>("seccion"));
    cantidad.setCellValueFactory(new PropertyValueFactory<>("cantidad"));
    urgente.setCellValueFactory(new PropertyValueFactory<>("urgente"));
    listaCompra.setItems(productos);
  }//initialize()
  
  public void mostrarFormulario(ActionEvent e) throws Exception {
    Parent pane = FXMLLoader.load(getClass().getResource(this.formulario));
    Stage stage = new Stage();
    Scene escena = new Scene(pane, 220, 220);
    stage.setTitle(this.titulo);
    stage.setScene(escena);
    stage.initModality(Modality.APPLICATION_MODAL);
    stage.show();
  }//botonHandler()
  
  public void eliminarTodo(ActionEvent e) throws Exception {
    productos.clear();
  }//eliminarTodo()
  
  public void eliminarSeleccionados(ActionEvent e) throws Exception {
    Iterator iterador = productos.iterator();
    while(iterador.hasNext()){
      Producto temp = (Producto) iterador.next();
      if(temp.getMarcado().selectedProperty().getValue()){
        iterador.remove();
      }
    }//while
  }//eliminarSeleccionados()
  
  public void imprimir(ActionEvent e) throws Exception {
    String impresion = "Cantidad Producto - Seccion - Urgente";
    for(Producto producto : productos){
      impresion += "\n" + producto;
    }
     txtImpresora = new Text(impresion);
    System.out.println(impresion);
    FlowPane pane = new FlowPane();
    pane.getChildren().add(txtImpresora);
    Stage stage = new Stage();
    Scene escena = new Scene(pane, 350, 400);
    stage.setTitle(this.titulo);
    stage.setScene(escena);
    stage.initModality(Modality.APPLICATION_MODAL);
    stage.show();
  }//imprimir()
  
  public static void agregarProducto(Producto producto){
    productos.add(producto);
  }//agregarProducto()
  
}//ProductoController
