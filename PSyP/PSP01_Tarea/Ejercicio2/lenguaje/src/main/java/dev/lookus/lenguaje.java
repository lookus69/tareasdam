package dev.lookus;

import java.io.FileWriter;

public class lenguaje 
{
    
    /** 
     * 
     * @param args
     */
    public static void main( String[] args ){
        if(args.length != 2){
            System.err.println("el numero de parametros es incorrecto");
            return;
        }
        int palabras = Integer.valueOf(args[0]);
        String nombreArchivo = args[1];
        try{
            FileWriter archivo = new FileWriter(nombreArchivo, true);
            for(int i = 0; i < palabras; i++){
                archivo.write(generarPalabra() + "\n");
            }
            archivo.close();
        }catch(Exception e){
            System.err.println("Ha ocurrido un error al intentar escribir el archivo: " + e.toString());
        }
        
    }

    
    /** 
     * genera una cadena de texto aleatorio entre 1 y 15 caracteres
     * @return String
     */
    private static String generarPalabra(){
        final String abc="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        int numCaracteres = (int)Math.round((Math.random() * 14) + 1); //genera un numero aleatorio entre 1 y 15
        String palabra = "";
        for(int i = 0; i < numCaracteres; i++){
            palabra += abc.charAt((int)Math.floor(Math.random() * abc.length()));
        }
        return palabra;
    }

}
