package dev.lookus;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author lookus
 */
public class ordenarNumeros {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        InputStreamReader entrada = new InputStreamReader(System.in);
        BufferedReader bufer = new BufferedReader(entrada);
        String linea;
        List<Integer> numeros = new ArrayList<>();
        try{
            while((linea = bufer.readLine()) != null){
                numeros.add(Integer.valueOf(linea));
            }
        }catch (Exception e){
            System.err.println("Se ha producido un error: " + e.toString());
        }
        Collections.sort(numeros);
        System.out.println(numeros);
    }
    
}